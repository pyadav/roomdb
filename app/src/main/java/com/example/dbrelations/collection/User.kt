package com.example.dbrelations.collection

import androidx.room.Entity

@Entity(tableName = "User",primaryKeys = ["id"])
data class User constructor(
    var created_at: String? = "",
    var domain_id: String? = "",
    var email: String? = "",
    var email_verified_at: String? = "",
    var first_name: String? = "",
    var full_name: String? = "",
    var has_set_password: Boolean,
    var has_used_sso: Int,
    var id: String = "1",
    var invitation_date: String? = "",
    var is_disabled: Int,
    var is_profile_verified: Int,
    var last_name: String? = "",
    var last_seen_at: String? = "",
    var linked_in_url: String? = "",
    var middle_name: String? = "",
    var mobile: String? = "",
    var owner_id: String? = "",
    var password_expires_at: String? = "",
    var profile_pic: String? = "",
    var role_id: String? = "",
    var twitter_url: String? = "",
    var updated_at: String? = ""
)
package com.example.dbrelations.collection

import androidx.room.Entity
import androidx.room.Ignore

@Entity (tableName = "Collection" ,primaryKeys =  ["id","image_id"])
data class Collection constructor(
    var _lft: Int=0,
    var _rgt: Int=0,
    var created_at:  String? = "",
    var description:  String? = "",
    var domain_id: String="1",
    var id: String="1",
    @Ignore var image: Image?=null,
    var image_id:  String = "1",
    @Ignore var main_resources: List<MainResource>?=null,
    var name: String?="",
    var owner_id: String?="",
    var parent_id: String?="",
    var updated_at: String?=""
)
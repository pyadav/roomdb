package com.example.dbrelations.collection

import androidx.room.Entity


data class CollectionsResultsResponse(
    val data: List<Data>,
    val links: Links,
    val message: String,
    val meta: Meta,
    val status: String,
    val status_code: Int
)
package com.example.dbrelations.collection.collectiondao

import androidx.room.Embedded
import androidx.room.Relation
import com.example.dbrelations.collection.Collection
import com.example.dbrelations.collection.Image

data class CollectionOneToOne  (
    @Embedded val collection: Collection,
    @Relation(
        parentColumn = "image_id",
        entityColumn = "id"
    )
    val image: Image
)
package com.example.dbrelations.collection

import androidx.room.Entity

@Entity(tableName = "Image" ,primaryKeys =  ["id"])
data class Image constructor(
    val created_at: String? = "",
    val extension: String? = "",
    val id: String = "1",
    val original_file_name: String? = "",
    val size: Int,
    val updated_at: String? = "",
    val url: String? = ""
)
package com.example.dbrelations.collection.manytomany

import androidx.room.Entity

@Entity(tableName = "ManyData", primaryKeys = ["songId","playlistId"])
data class Data constructor(
    val playlistId: Int,
    val songId: Int

)
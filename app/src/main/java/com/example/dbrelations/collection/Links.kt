package com.example.dbrelations.collection

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "CollectionLinks")
data class Links constructor(
    @PrimaryKey(autoGenerate = true)
    var id:Int=1,
    var first:String? = "",
    var last: String? = "",
    var next: String? = "",
    var prev: String? = ""
)
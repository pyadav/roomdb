package com.example.dbrelations.collection.collectiondao

import androidx.room.*
import com.example.dbrelations.ContactsResponseModel.Contact
import com.example.dbrelations.Playlist
import com.example.dbrelations.PlaylistWithSongs
import com.example.dbrelations.Song
import com.example.dbrelations.SongWithPlaylists
import com.example.dbrelations.collection.*
import com.example.dbrelations.collection.Collection
import java.io.DataOutputStream

@Dao
interface CollectionDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollection(collection: Collection)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionData(data: Data)

    //Many to Many
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertManyToManyData(data: com.example.dbrelations.collection.manytomany.Data)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSongData(song: Song)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPLayListData(playlist: Playlist)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionImage(image: Image)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectonMainResources(mainResource: MainResource)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionLinks(links: Links)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionMainResource(mainResource: MainResource)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionMeta(meta: Meta)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollectionUser(user: User)

    @Transaction
    fun insertCollectionResponseData(collectionsResultsResponse: Data){
        insertCollectionData(collectionsResultsResponse)
        collectionsResultsResponse?.collection?.let { insertCollection(it) }
        collectionsResultsResponse.collection?.image?.let { insertCollectionImage(it) }
        insertCollectionMainResourceData(collectionsResultsResponse.collection?.main_resources)
    }

    //ManyToManyinsertion
    @Transaction
    fun insertManyToManyResponseData(manytomanyResultsResponse: com.example.dbrelations.collection.manytomany.Data){
        insertManyToManyData(manytomanyResultsResponse)
    }

    @Transaction
    @Query("SELECT * FROM Playlist")
    fun getPlaylistsWithSongs(): List<PlaylistWithSongs>

    @Transaction
    @Query("SELECT * FROM Song")
    fun getSongsWithPlaylists(): List<SongWithPlaylists>

    @Transaction
    fun insertSongResponseData(song:Song){
        insertSongData(song)
    }
    @Transaction
    fun insertPLayListesponseData(playlist: Playlist){
        insertPLayListData(playlist)
    }


    @Transaction
    fun insertSongResponseData(manytomanyResultsResponse: com.example.dbrelations.collection.manytomany.Data){
        insertManyToManyData(manytomanyResultsResponse)
    }

    fun insertCollectionMainResourceData(mainResources: List<MainResource>?) {
        mainResources?.forEach {
            insertCollectionMainResource(it)
        }

    }

    //one to one
    @Transaction
    @Query("SELECT * FROM Collection")
    fun getCollectionAndImages(): List<CollectionOneToOne>


    @Transaction
    @Query("SELECT * FROM Collection")
    fun getCollectionAndMainResources(): List<CollectionOneToMany>





}
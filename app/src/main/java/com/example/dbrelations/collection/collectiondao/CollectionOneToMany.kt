package com.example.dbrelations.collection.collectiondao

import androidx.room.Embedded
import androidx.room.Relation
import com.example.dbrelations.collection.Collection
import com.example.dbrelations.collection.Image
import com.example.dbrelations.collection.MainResource

data class CollectionOneToMany (@Embedded val collection: Collection,
                                @Relation(
                                     parentColumn = "domain_id",
                                     entityColumn = "domain_id"
                                 )
                                 val mainResource: List<MainResource>,
                                @Relation(
                                    parentColumn = "image_id",
                                    entityColumn = "id"
                                )
                                val image: Image)
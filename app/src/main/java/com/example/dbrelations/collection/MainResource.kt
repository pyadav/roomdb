package com.example.dbrelations.collection

import androidx.room.Entity

@Entity(tableName = "MainResource" ,primaryKeys =  ["id"])
data class MainResource constructor(
    var cms_folder_id: String? = "",
    var copied_from: String? = "",
    var created_at: String? = "",
    var description: String? = "",
    var domain_id: String? = "",
    var expected_time: String? = "",
    var expiry_date: String? = "",
    var id: String = "1",
    var is_downloadable: Int,
    var is_favouritable: Int,
    var is_presentable: Int,
    var is_printable: Int,
    var is_shareable: Int,
    var name: String? = "",
    var owner_id: String? = "",
    var resourceable_id: String? = "",
    var resourceable_type: String? = "",
    var updated_at: String? = ""
)
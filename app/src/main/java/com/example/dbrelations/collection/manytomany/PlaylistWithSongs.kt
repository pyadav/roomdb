package com.example.dbrelations.collection.manytomany

import androidx.room.*
import com.example.dbrelations.Playlist
import com.example.dbrelations.Song

class PlaylistWithSongs (
    @Embedded val playlist: Playlist,
    @Relation(
         parentColumn = "playlistId",
       entityColumn = "songId",
       associateBy = Junction(Data::class)
)
    val songs: List<Song>
)


package com.example.dbrelations.collection

import androidx.room.Entity
import androidx.room.Ignore

@Entity(tableName = "CollectionData", primaryKeys = ["id","collection_id","user_id"])
data class Data constructor(
    @Ignore var collection: Collection?=null,
    var collection_id: String="1",
    var created_at: String? = "",
    var id: String="1",
    var last_visited_at:String? = "",
    var strategy: String? = "",
    var strategy_id: String? = "",
    var updated_at: String? = "",
    @Ignore var user: User?=null,
    var user_id: String="1"
)
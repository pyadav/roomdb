package com.example.dbrelations.collection

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Meta")
data class Meta constructor(
    @PrimaryKey(autoGenerate = true)
    var id:Int=1,
    var current_page: Int=-1,
    var from: Int=-1,
    var last_page: Int=-1,
    var path: String? = "",
    var per_page: Int=-1,
    var to: Int=-1,
    var total: Int=-1
)
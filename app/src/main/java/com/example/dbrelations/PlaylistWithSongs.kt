package com.example.dbrelations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.dbrelations.collection.manytomany.Data

data class PlaylistWithSongs(
    @Embedded val playlist: Playlist,
    @Relation(
        parentColumn = "playlistId",
        entityColumn = "songId",
       associateBy = Junction(Data::class)
    )
    val songs: List<Song>
)
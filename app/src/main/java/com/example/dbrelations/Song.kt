package com.example.dbrelations

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Song", primaryKeys = ["songId"])
data class Song constructor(
     var songId: Int,
    var songName: String

)
package com.example.dbrelations

import androidx.room.*

class UserWithPlaylistsAndSongs {
    @Embedded val user: User? =null
    @Relation(
        entity = Playlist::class,
        parentColumn = "userId",
        entityColumn = "userCreatorId"
    )
    val playlists: List<PlaylistWithSongs>? =null
}


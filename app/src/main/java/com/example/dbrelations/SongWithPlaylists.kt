package com.example.dbrelations

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.dbrelations.collection.manytomany.Data

data class SongWithPlaylists (
    @Embedded val song: Song,
    @Relation(
        parentColumn = "songId",
        entityColumn = "playlistId",
        associateBy = Junction(Data::class)
    )
    val playlists: List<Playlist>
)
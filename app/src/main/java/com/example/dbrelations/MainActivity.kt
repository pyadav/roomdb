package com.example.dbrelations

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.dbrelations.ContactsResponseModel.ContactsResponse
import com.example.dbrelations.collection.*
import com.example.dbrelations.collection.Collection
import com.example.dbrelations.collection.manytomany.SongsWithPlayList
import com.google.gson.Gson


class MainActivity : AppCompatActivity() {
    lateinit var appDatabase: AppDatabase

    var response : String = "{\n" +
            "  \"contacts\": [\n" +
            "    {\n" +
            "      \"id\": \"c200\",\n" +
            "      \"name\": \"Ravi Tamada\",\n" +
            "      \"email\": \"ravi@gmail.com\",\n" +
            "      \"address\": \"xx-xx-xxxx,x - street, x - country\",\n" +
            "      \"phoneId\": \"111l̥\",\n" +
            "      \"gender\": \"male\",\n" +
            "      \"phone\": {\n" +
            "        \"id\": \"111l̥\",\n" +
            "        \"mobile\": \"+91 0000000000\",\n" +
            "        \"home\": \"00 000000\",\n" +
            "        \"office\": \"00 000000\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"c201\",\n" +
            "      \"name\": \"Johnny Depp\",\n" +
            "      \"email\": \"johnny_depp@gmail.com\",\n" +
            "      \"address\": \"xx-xx-xxxx,x - street, x - country\",\n" +
            "      \"phoneId\": \"2222\",\n" +
            "      \"gender\": \"male\",\n" +
            "      \"phone\": {\n" +
            "        \"id\": \"2222\",\n" +
            "        \"mobile\": \"+91 0000000000\",\n" +
            "        \"home\": \"00 000000\",\n" +
            "        \"office\": \"00 000000\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"c202\",\n" +
            "      \"name\": \"Leonardo Dicaprio\",\n" +
            "      \"email\": \"leonardo_dicaprio@gmail.com\",\n" +
            "      \"address\": \"xx-xx-xxxx,x - street, x - country\",\n" +
            "      \"phoneId\": \"33333\",\n" +
            "      \"gender\": \"male\",\n" +
            "      \"phone\": {\n" +
            "        \"id\": \"3333\",\n" +
            "        \"mobile\": \"+91 0000000000\",\n" +
            "        \"home\": \"00 000000\",\n" +
            "        \"office\": \"00 000000\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var gson = Gson()
        var contactsResponse = gson.fromJson(response,ContactsResponse::class.java)

        appDatabase = AppDatabase.getDatabase(applicationContext)

        collectionInserData()
        manyTomany()
       /*AsyncTask.execute {
           contactsResponse.contacts.forEach {
               var item = appDatabase.contactsDao().insertContactData(it)

           }


           Log.v("TAG",""+appDatabase.contactsDao().getUserListWithPhoneNumber()[0])
           Log.v("TAG",""+Gson().toJson(appDatabase.contactsDao().getUserListWithPhoneNumber()[0]))



       }*/


    }

    private fun manyTomany() {
        var songsWithPlayList:SongsWithPlayList=  Gson().fromJson(
            AppDatabase.loadJSONFromAsset(applicationContext,"manytomany.json"),
            SongsWithPlayList::class.java
        )
        AsyncTask.execute {
            for (i in 1..5) {
                var song = Song(i, "song" + i)
                var playlist = Playlist(i, "playListName" + i)
                var item = appDatabase.collectionDataDao().insertSongResponseData(song)
                appDatabase.collectionDataDao().insertPLayListData(playlist)
            }
        }
       // Log.v("TAG-songsWithPlayList",""+songsWithPlayList.data.get(0).songname)

        AsyncTask.execute {
            songsWithPlayList.data.forEach {


                var item = appDatabase.collectionDataDao().insertManyToManyResponseData(it)

            }
        }

        //Many to Many PLaylist display songs
        getManyToManyDataPlayListWithSong()
        getManyToManyDataSongsWithPlayList()

    }

    private fun getManyToManyDataSongsWithPlayList() {
        AsyncTask.execute {

            var item = appDatabase.collectionDataDao().getSongsWithPlaylists()
            var myPlayListName="test"
            Log.d("getSongsWithPlayList-",Gson().toJson(item))
            item.forEach {
                Log.d("getSongPlayList",it.song.songId.toString()+"-@@->")
                myPlayListName=it.song.songName
                it.playlists.forEach {
                    Log.d("getSongPlayList@",it.playlistName+"-->")
                }
            }

        }
    }

    private fun getManyToManyDataPlayListWithSong() {
        //one-to-one
        AsyncTask.execute {

            var item = appDatabase.collectionDataDao().getPlaylistsWithSongs()
           var myPlayListName="test"
            Log.d("getPlayList-obj-",Gson().toJson(item))
            item.forEach {
                Log.d("getPlayList",it.playlist.playlistName+"-@@->")
                myPlayListName=it.playlist.playlistName
                it.songs.forEach {
                    Log.d("getPlay-Song-List",it.songName+"-->")
                }
            }

        }
    }

    fun collectionInserData()
    {

        //
        var collectionsResultsResponse:CollectionsResultsResponse=  Gson().fromJson(
            AppDatabase.loadJSONFromAsset(applicationContext,"collection_parent.json"),
            CollectionsResultsResponse::class.java
        )

        Log.v("TAG-collectionsResultse",""+collectionsResultsResponse.data.get(0).collection?.name)
        //

        AsyncTask.execute {
            collectionsResultsResponse.data.forEach {
                var item = appDatabase.collectionDataDao().insertCollectionResponseData(it)

            }
        }


        //one-to-one
        AsyncTask.execute {

            var item = appDatabase.collectionDataDao().getCollectionAndImages()

            item.forEach {
                Log.d("getItemsList",it.collection.name+"-->"+it.image.id)
            }
        }

        //one-to-many
        AsyncTask.execute {
            var data=Data()
            var collectObj:Collection
            var image:Image
            //var listMainResources:List<MainResource>=ArrayList
            var listMainResources = ArrayList<MainResource>()
            var collectionList = ArrayList<Data>()
            var item = appDatabase.collectionDataDao().getCollectionAndMainResources()


            item.forEach {
                var collectionName:String=""
                //Log.d("one-to-Many",it.collection.name+"-->"+it.mainResource.get(0).id)
                Log.d("one-to-Many-Image",it.image.id)

                collectionName= it.collection.name.toString()
                /*collectObj=it.collection
                image=it.image*/
                data.collection=it.collection
                data.collection!!.image=it.image
                it.mainResource.forEach {
                    Log.d("one-to-Many-"+collectionName,it.id)
                    listMainResources.add(it)
                }
                data.collection!!.main_resources=listMainResources

                collectionList.add(data)
            }

            var collectionListResult=Gson().toJson(collectionList)
            Log.d("collection-Json-",collectionListResult.toString())
        }






    }


}
package com.example.dbrelations.ContactsResponseModel

import androidx.room.Embedded
import androidx.room.Relation

data class ContactsWithPhone(
      @Embedded var contact : Contact,
        @Relation(parentColumn = "phoneId", entityColumn = "id", entity = Phone::class)
        var phone:List<Phone>?=null
)



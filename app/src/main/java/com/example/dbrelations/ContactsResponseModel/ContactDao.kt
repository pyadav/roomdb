package com.example.dbrelations.ContactsResponseModel

import androidx.room.*

@Dao
interface ContactDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContact(contact: Contact)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhone(phone: Phone)


    @Transaction
    @Query("SELECT * FROM Contact")
    fun getUserListWithPhoneNumber(): List<ContactsWithPhone>

    @Transaction
    fun insertContactData(contact: Contact){
        insertContact(contact)
        insertPhone(contact.phone!!)
    }


}
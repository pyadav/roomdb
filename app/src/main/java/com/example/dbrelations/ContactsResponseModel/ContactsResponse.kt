package com.example.dbrelations.ContactsResponseModel


data class ContactsResponse(
    var contacts: List<Contact>
)
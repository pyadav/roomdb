package com.example.dbrelations.ContactsResponseModel

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "Phone")
data class Phone(
    var home: String? = "",
    var mobile: String? = "",
    var office: String? = "",
    @PrimaryKey(autoGenerate = false)
    var id: String= "123"
)
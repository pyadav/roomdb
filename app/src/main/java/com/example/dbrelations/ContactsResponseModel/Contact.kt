package com.example.dbrelations.ContactsResponseModel

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "Contact", primaryKeys = ["id", "phoneId"])
data class Contact constructor(
    var address: String? = "",
    var email: String? = "",
    var gender: String? = "",
    var id: String = "123",
    var name: String? = "",
    var phoneId: String ="1234",
    @Ignore var phone: Phone? = null
)
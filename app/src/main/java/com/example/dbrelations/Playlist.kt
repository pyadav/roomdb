package com.example.dbrelations

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Playlist", primaryKeys = ["playlistId"])
data class Playlist(
    var playlistId: Int=1,
    var playlistName: String
)
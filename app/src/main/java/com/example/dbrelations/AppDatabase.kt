package com.example.dbrelations

import android.content.Context
import androidx.room.Database
import androidx.room.Entity
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.dbrelations.ContactsResponseModel.Contact
import com.example.dbrelations.ContactsResponseModel.ContactDao
import com.example.dbrelations.ContactsResponseModel.Phone
import com.example.dbrelations.collection.*
import com.example.dbrelations.collection.Collection
import com.example.dbrelations.collection.User
import com.example.dbrelations.collection.collectiondao.CollectionDataDao
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

@Database(entities = [Phone::class,Contact::class,Collection::class,Data::class,Image::class,Links::class,MainResource::class,Meta::class,User::class,com.example.dbrelations.collection.manytomany.Data::class,Song::class,Playlist::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactsDao(): ContactDao
    abstract fun collectionDataDao(): CollectionDataDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDatabase? = null

        public fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "AppDatabase.db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }

        fun loadJSONFromAsset(context: Context,fileName: String): String? {
            var json: String? = null
            json = try {
                val `is`: InputStream = context.applicationContext.assets.open(fileName)
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                String(buffer, Charset.forName("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }
            return json
        }
    }




}
